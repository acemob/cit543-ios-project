//
//  MapViewController.h
//  Food Searcher
//
//  Created by E-CITY Tan on 11/6/12.
//  Copyright (c) 2012 CIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "SelectedFood.h"
#import <CoreLocation/CoreLocation.h>

@interface MapViewController : UIViewController <MKMapViewDelegate>
{
    CLLocationManager *locationManager;
}


- (void)mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation;

@property (strong, nonatomic) IBOutlet MKMapView *mapView;

@end
