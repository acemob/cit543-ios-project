//
//  AppDelegate.h
//  Food Searcher
//
//  Created by E-CITY Tan on 10/27/12.
//  Copyright (c) 2012 CIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
