//
//  Food.m
//  Food Searcher
//
//  Created by E-CITY Tan on 11/4/12.
//  Copyright (c) 2012 CIT. All rights reserved.
//

#import "Food.h"

@implementation Food

@synthesize POI_ID;
@synthesize POI_Name;
@synthesize Latitude;
@synthesize Longitude;
@synthesize Description;
@synthesize Thumb;
@synthesize Picture;

@end