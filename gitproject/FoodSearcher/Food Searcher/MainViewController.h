//
//  MainViewController.h
//  Food Searcher
//
//  Created by E-CITY Tan on 11/8/12.
//  Copyright (c) 2012 CIT. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MainViewController : UIViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *nextBarButton;

@end
