//
//  VBAnnotation.h
//  Food Searcher
//
//  Created by E-CITY Tan on 11/6/12.
//  Copyright (c) 2012 CIT. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface VBAnnotation : NSObject <MKAnnotation>

@property (nonatomic , assign)CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

- initWithPosition:(CLLocationCoordinate2D) coords;

@end
