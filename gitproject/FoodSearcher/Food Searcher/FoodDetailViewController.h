//
//  FoodDetailViewController.h
//  Food Searcher
//
//  Created by E-CITY Tan on 10/27/12.
//  Copyright (c) 2012 CIT. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectedFood.h"

@interface FoodDetailViewController : UIViewController

@property (nonatomic, strong) NSString *foodName;
@property (strong, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) IBOutlet UITextView *descriptionLabel;
@property (strong, nonatomic) IBOutlet UIImageView *foodImage;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *mapBarButton;

@end
