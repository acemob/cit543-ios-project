//
//  VBAnnotation.m
//  Food Searcher
//
//  Created by E-CITY Tan on 11/6/12.
//  Copyright (c) 2012 CIT. All rights reserved.
//

#import "VBAnnotation.h"

@implementation VBAnnotation

@synthesize coordinate;
@synthesize title;
@synthesize subtitle;

-(id)initWithPosition:(CLLocationCoordinate2D)coords
{
    if(self = [super init])
    {
        self.coordinate = coords;
    }
    return self;
}
@end
